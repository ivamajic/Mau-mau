class Karta {
  constructor (boja) {
    this.boja=boja;
    this.vrijednost;
  }
}

let karte=[];
let rukaIgrac=[];
let rukaComp=[];
let odbacene=[];
let asflag=false;
let brojac7=0;

//postavljanje atributa ma
for(let i=0; i<8; i++) {
  karte[i]=new Karta("srce");
}
for(let i=8; i<16; i++) {
  karte[i]=new Karta("list");
}
for (let i=16; i<24; i++) {
  karte[i]=new Karta("žir");
}
for (let i=24; i<32; i++) {
  karte[i]=new Karta("bundeva");
}
for (let i=0; i<32; i++) {
  if (i===0 || i===8 || i===16 || i===24) {
    karte[i].vrijednost="sedam";
  }
  else if (i===1 || i===9 || i===17 || i===25) {
    karte[i].vrijednost="osam";
  }
  else if (i===2 || i===10 || i===18 || i===26) {
    karte[i].vrijednost="devet";
  }
  else if (i===3 || i===11 || i===19 || i===27) {
    karte[i].vrijednost="deset";
  }
  else if (i===4 || i===12 || i===20 || i===28) {
    karte[i].vrijednost="decko";
  }
  else if (i===5 || i===13 || i===21 || i===29) {
    karte[i].vrijednost="dama";
  }
  else if (i===6 || i===14 || i===22 || i===30) {
    karte[i].vrijednost="kralj";
  }
  else {
    karte[i].vrijednost="as";
  }
}

//mijesanje karata u spilu
function mijesanje() {
  for (let i = 0; i < karte.length; i++) {
    let j = Math.round(Math.random()*(31 - i) + i);
    let temp = karte[i];
    karte[i] = karte[j];
    karte[j] = temp;
  }
}

function odaberiKartu(gornjaKarta) {
  if (gornjaKarta.vrijednost==="sedam") { //ako je bačena sedmica, AI želi pobiti tu sedmicu
    for (let i=0; i<rukaComp.length; i++) {
      if (rukaComp[i].vrijednost==="sedam") {
        odabranaKarta=rukaComp[i];
        return odabranaKarta;
      }
    }
  }
  else if (gornjaKarta.vrijednost==="as") { //ako je bačen as, igrač je ponovno na redu te želi baciti još jedan as
    for (let i=0; i<rukaComp.length; i++) {
      if (rukaComp[i].vrijednost==="as") {
        odabranaKarta=rukaComp[i];
        return odabranaKarta;
      }
    }
  }
  else {  //ako je bačeno bilo što drugo
    //ako ima barem dvije sedmice, zeli baciti sedmicu
    let brojac=0;
    for (let i=0; i<rukaComp.length; i++) {
      if(rukaComp[i].vrijednost==="sedam") {
        brojac++;
      }
    }
    if (brojac>=2) {
      for (let i=0; i<rukaComp.length; i++) {
        if (rukaComp[i].vrijednost==="sedam" && rukaComp[i].boja===gornjaKarta.boja) {
          odabranaKarta=rukaComp[i];
          brojac=0;
          return odabranaKarta;
        }
      }
    }
  }
  //želi baciti as ako ima još nešto iste boje(2.) ili još jedan as i nešto te druge boje(1.)
  let asevi=[];
  for (let i=0; i<rukaComp.length; i++) { //(1.) - ima li bar dva asa
    if (rukaComp[i].vrijednost==="as") {
      brojac++;
      asevi.push(rukaComp[i].boja);
    }
  }
  if (brojac>=2) { //ako ima bar dva asa
    for (let i=0; i<rukaComp.length; i++) { //ima li još nešto iste boje kao i drugi as
      for (let j=0; j<asevi.length; j++) {
        if (asevi[j].boja===rukaComp[i].boja && rukaComp[i].vrijednost!="as" && rukaComp[i].boja != gornjaKarta.boja) {
          for (let k=0; k<rukaComp.length; k++) {
            if (rukaComp[k].vrijednost==="as" && rukaComp[k].boja===gornjaKarta.boja) {
              brojac=0;
              return rukaComp[k];
            }
          }
        }
      }
    }
  }
  //zeli baciti osmicu
  for (let i=0; i<rukaComp.length; i++) {
    if (rukaComp[i].boja===gornjaKarta.boja && rukaComp[i].vrijednost==="osam") {
      return rukaComp[i];
    }
  }
  //zeli baciti kartu iste boje ili vrijednosti
  for (let i=0; i<rukaComp.length; i++) {
    if ((rukaComp[i].boja===gornjaKarta.boja || rukaComp[i].vrijednost===gornjaKarta.vrijednost) && rukaComp[i].vrijednost != "decko") {
      return rukaComp[i];
    }
  }
  //zeli baciti decka ako nema vise nista te boje
  let flag=false;
  for (let i=0; i<rukaComp.length; i++) {
    if (rukaComp[i].boja===gornjaKarta.boja || rukaComp[i].vrijednost===gornjaKarta.vrijednost) {
      flag=true;
    }
  }
  if (flag===false) {
    for (let i=0; i<rukaComp.length; i++) {

    }
  }
}

//potez igrača
function potezIgrac(gornjaKarta) { //funkcija treba vratiti vrijednost nove gornjeKarte
  let odabranaKartaIgrac = 0;//karta na koju je igrač kliknuo, OVO TREBA SKONTATI KAKO SE RADI
  if ((odabranaKartaIgrac.vrijednost===gornjaKarta.vrijednost || odabranaKartaIgrac.boja===gornjaKarta.boja) && odabranaKartaIgrac.vrijednost==="sedam") {
    //izbaci odabranu kartu iz ruke
    brojac7 += 2; //dodaju se dvije karte koje sljedeći igrač treba izvući
    return odabranaKartaIgrac;
  }
  else if (brojac7 !== 0) { //ako igrač nije bacio sedmicu, a igrač prije toga jest
    for (let i=0; i<brojac7; i++) {
      rukaIgrac.push(karte[karte.length-1]);
    }
    for (let i=0; i<brojac7; i++) {
      karte.pop();
    }
    brojac7 = 0;
  }

  if ((odabranaKartaIgrac.vrijednost===gornjaKarta.vrijednost || odabranaKartaIgrac.boja===gornjaKarta.boja) && odabranaKartaIgrac.vrijednost==="osam") {
    for (let i=0; i<rukaIgrac.length; i++) {
      //izbaci odabranu kartu iz ruke
      if (rukaIgrac[i].boja === odabranaKartaIgrac.boja) {
        //izbaci kartu iz ruke
        odabranaKartaIgrac=rukaIgrac[i];
      }
    }
    return odabranaKartaIgrac;
  }

  else if (odabranaKartaIgrac.vrijednost==="decko") {
    if (odabranaKartaIgrac.boja != gornjaKarta.boja) {
      //izbaci odabranuKartu iz ruke
    }

    else {
      //omoguci izbor nove boje koju se treba baciti, a koja je različita od odabranaKarta.boja tj. gornjaKarta.boja
      //izbaci odabranuKartu iz ruke
    }
    return odabranaKartaIgrac;
  }
  else if ((odabranaKartaIgrac.vrijednost===gornjaKarta.vrijednost || odabranaKartaIgrac.boja===gornjaKarta.boja) && odabranaKartaIgrac.vrijednost==="as") {
    asflag=true;
    //izbaci odabranuKartu iz ruke splice(?) ili nekako
    return odabranaKartaIgrac; //bačena karta postaje gornjaKarta

  }
  else if (odabranaKartaIgrac.vrijednost===gornjaKarta.vrijednost || odabranaKartaIgrac.boja===gornjaKarta.boja) {
    //izbaci kartu iz ruke
    return odabranaKartaIgrac;
  }
  else {
    //omoguci povlacenje karte iz spila karte[] sve dok ne dobije nesto sto se moze baciti
    let brojacKarata=0;
    do {
      rukaIgrac.push(karte[karte.length-1]);
      brojacKarata++;
    } while (karte[karte.length-1].boja!=gornjaKarta.boja && karte[karte.length-1].vrijednost!=gornjaKarta.vrijednost && karte[karte.length-1].vrijednost!="decko");
    for (let i=0; i<brojacKarata; i++) {
      karte.pop();
    }
    potezIgrac(gornjaKarta);
  }
}

//potez računala
function potezComp(gornjaKarta) {
  let odabranaKartaComp = odaberiKartu(gornjaKarta);//karta na koju je igrač kliknuo, OVO TREBA SKONTATI KAKO SE RADI

  if (odabranaKartaComp.vrijednost==="sedam") {
    //izbaci odabranu kartu iz ruke
    brojac7 += 2; //dodaju se dvije karte koje sljedeći igrač treba izvući
    return odabranaKartaComp;
  }
  else if (brojac7 !== 0) { //ako igrač nije bacio sedmicu, a igrač prije toga jest
    for (let i=0; i<brojac7; i++) {
      rukaComp.push(karte[karte.length-1]);
    }
    for (let i=0; i<brojac7; i++) {
      karte.pop();
    }
    brojac7 = 0;
  }

  if (odabranaKartaComp.vrijednost==="osam") {
    //izbaci odabranu kartu iz ruke
    for (let i=0; i<rukaComp.length; i++) {
      if (rukaComp[i].boja === odabranaKartaComp.boja) {
        //izbaci kartu iz ruke
        odabranaKartaComp=rukaComp[i];
      }
    }
    return odabranaKartaComp;
  }

  else if (odabranaKartaComp.vrijednost==="decko") {
    if (odabranaKartaComp.boja != gornjaKarta.boja) {
      //izbaci odabranuKartu iz ruke
    }

    else {
      //omoguci izbor nove boje koju se treba baciti, a koja je različita od odabranaKarta.boja tj. gornjaKarta.boja
      //izbaci odabranuKartu iz ruke
    }
    return odabranaKartaComp;
  }
  else if (odabranaKartaComp.vrijednost==="as") {
    asflag=true;
    //izbaci odabranuKartu iz ruke splice(?) ili nekako
    return odabranaKartaComp; //bačena karta postaje gornjaKarta

  }
  else if (odabranaKartaComp.vrijednost===gornjaKarta.vrijednost || odabranaKartaComp.boja===gornjaKarta.boja) {
    //izbaci kartu iz ruke
    return odabranaKartaComp;
  }
  else {
    //omoguci povlacenje karte iz spila karte[] sve dok ne dobije nesto sto se moze baciti
    let brojacKarata=0;
    do {
      rukaIgrac.push(karte[karte.length-1]);
      brojacKarata++;
    } while (karte[karte.length-1].boja!=gornjaKarta.boja && karte[karte.length-1].vrijednost!=gornjaKarta.vrijednost && karte[karte.length-1].vrijednost!="decko");
    for (let i=0; i<brojacKarata; i++) {
      karte.pop();
    }
    potezComp(gornjaKarta);
  }
}

function promijesajSpil() {
  for (let i=0; i<odbacene.length; i++) {
    karte.push(odbacene[i]);
  }
  mijesanje();
}


function main() {
  mijesanje();
  //podjela karata: 7 igracu i 7 racunalu (drugom igracu)
  for(let i=31; i>24; i--) {
    rukaIgrac.push(karte[i]);
    rukaComp.push(karte[i-7]);
  }
  //iz početnog špila izbacuju se podijeljene karte
  karte.splice(19, 14);
  //postavljanje prve karte na igraće polje, također se ta karta izbacuje iz špila
  let gornjaKarta=karte[karte.length-1];
  odbacene.push(karte[karte.length-1]);
  karte.pop();
  //potezi, tj. igra - loop koji se izvodi dok jedan od igrača ne ostane bez karata u ruci
  do {
    if (asflag===false) {
      gornjaKarta=potezIgrac(gornjaKarta);
      if (rukaIgrac.length === 0) {
        return;
      }
    }
    else asflag=false;
    if (asflag===false) {
      gornjaKarta=potezComp(gornjaKarta);
      if (rukaComp.length === 0) {
        return;
      }
    }
    else asflag=false;
    if (karte.length===0) {
      promijesajSpil();
    }
  } while (rukaComp.length !== 0 || rukaIgrac.length !== 0);

}
