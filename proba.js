class Karta {
  constructor (boja) {
    this.boja=boja;
    this.vrijednost;
    this.slika;
    this.bacena=false;
  }
}

let karte=[];
let rukaIgrac=[];
let rukaComp=[];
let odbacene=[];
let asflag=false;
let brojac7=0;
let gornjaKarta;
let igracFlag;
let compFlag;
let pauza=false;
let novaBoja;
let novaFlag=false;
let igracBrojac = 0;
let compBrojac = 0; // ove dvije varijable povecaju se svaki put kad izvuce kartu, smanje kad baci


//postavljanje atributa kartama
for(let i=0; i<8; i++) {
  karte[i]=new Karta("srce");
}
for(let i=8; i<16; i++) {
  karte[i]=new Karta("list");
}
for (let i=16; i<24; i++) {
  karte[i]=new Karta("zir");
}
for (let i=24; i<32; i++) {
  karte[i]=new Karta("bundeva");
}

for (let i=0; i<32; i++) {
  switch(i) {
    case 0:
      karte[i].vrijednost="sedam";
      karte[i].slika="images\\srce-sedam.gif";
      break;
    case 1:
      karte[i].vrijednost="osam";
      karte[i].slika="images\\srce-osam.gif";
      break;
    case 2:
      karte[i].vrijednost="devet";
      karte[i].slika="images\\srce-devet.gif";
      break;
    case 3:
    karte[i].vrijednost="deset";
    karte[i].slika="images\\srce-deset.gif";
    break;
    case 4:
    karte[i].vrijednost="decko";
    karte[i].slika="images\\srce-decko.gif";
    break;
    case 5:
    karte[i].vrijednost="dama";
    karte[i].slika="images\\srce-dama.gif";
    break;
    case 6:
    karte[i].vrijednost="kralj";
    karte[i].slika="images\\srce-kralj.gif";
    break;
    case 7:
    karte[i].vrijednost="as";
    karte[i].slika="images\\srce-as.gif";
    break;
    case 8:
    karte[i].vrijednost="sedam";
    karte[i].slika="images\\list-sedam.gif";
    break;
    case 9:
    karte[i].vrijednost="osam";
    karte[i].slika="images\\list-osam.gif";
    break;
    case 10:
    karte[i].vrijednost="devet";
    karte[i].slika="images\\list-devet.gif";
    break;
    case 11:
    karte[i].vrijednost="deset";
    karte[i].slika="images\\list-deset.gif";
    break;
    case 12:
    karte[i].vrijednost="decko";
    karte[i].slika="images\\list-decko.gif";
    break;
    case 13:
    karte[i].vrijednost="dama";
    karte[i].slika="images\\list-dama.gif";
    break;
    case 14:
    karte[i].vrijednost="kralj";
    karte[i].slika="images\\list-kralj.gif";
    break;
    case 15:
    karte[i].vrijednost="as";
    karte[i].slika="images\\list-as.gif";
    break;
    case 16:
    karte[i].vrijednost="sedam";
    karte[i].slika="images\\zir-sedam.gif";
    break;
    case 17:
    karte[i].vrijednost="osam";
    karte[i].slika="images\\zir-osam.gif";
    break;
    case 18:
    karte[i].vrijednost="devet";
    karte[i].slika="images\\zir-devet.gif";
    break;
    case 19:
    karte[i].vrijednost="deset";
    karte[i].slika="images\\zir-deset.gif";
    break;
    case 20:
    karte[i].vrijednost="decko";
    karte[i].slika="images\\zir-decko.gif";
    break;
    case 21:
    karte[i].vrijednost="dama";
    karte[i].slika="images\\zir-dama.gif";
    break;
    case 22:
    karte[i].vrijednost="kralj";
    karte[i].slika="images\\zir-kralj.gif";
    break;
    case 23:
    karte[i].vrijednost="as";
    karte[i].slika="images\\zir-as.gif";
    break;
    case 24:
    karte[i].vrijednost="sedam";
    karte[i].slika="images\\bundeva-sedam.gif";
    break;
    case 25:
    karte[i].vrijednost="osam";
    karte[i].slika="images\\bundeva-osam.gif";
    break;
    case 26:
    karte[i].vrijednost="devet";
    karte[i].slika="images\\bundeva-devet.gif";
    break;
    case 27:
    karte[i].vrijednost="deset";
    karte[i].slika="images\\bundeva-deset.gif";
    break;
    case 28:
    karte[i].vrijednost="decko";
    karte[i].slika="images\\bundeva-decko.gif";
    break;
    case 29:
    karte[i].vrijednost="dama";
    karte[i].slika="images\\bundeva-dama.gif";
    break;
    case 30:
    karte[i].vrijednost="kralj";
    karte[i].slika="images\\bundeva-kralj.gif";
    break;
    case 31:
    karte[i].vrijednost="as";
    karte[i].slika="images\\bundeva-as.gif";
    break;
  }
}

//mijesanje karata u spilu
function mijesanje() {
  for (let i = 0; i < karte.length; i++) {
    let j = Math.round(Math.random()*((karte.length-1) - i) + i);
    let temp = karte[i];
    karte[i] = karte[j];
    karte[j] = temp;
  }
}

function postaviGornju(gornjaKarta) {
  $(".gornja-area").empty().append($('<input type="image" id="gornja" width="98" height="150" />'));
  document.getElementById('gornja').src=gornjaKarta.slika;
}

//potez igraca
function potezIgrac(odabranaKarta) {

  if (odabranaKarta.vrijednost==gornjaKarta.vrijednost || (novaFlag==false && odabranaKarta.boja==gornjaKarta.boja) || (novaFlag==true && odabranaKarta.boja==novaBoja)) {
    novaFlag=false;
    //ako igrač odabere sedmicu
    if (odabranaKarta.vrijednost == "sedam") {
      brojac7 += 2;
    //  console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKarta;
      igracBrojac--;
    }

    //ako igrač odabere osmicu
    else if (odabranaKarta.vrijednost == "osam") {
  //    console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKarta;
      igracBrojac--;

      for (let i=0; i<rukaIgrac.length; i++) {
        if (rukaIgrac[i].boja == odabranaKarta.boja && rukaIgrac[i].bacena == false) {
          setTimeout( function() {
            rukaIgrac[i].bacena=true;
            document.getElementById(i).remove();
      //      console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
            odbacene.push(gornjaKarta);
            gornjaKarta=rukaIgrac[i];
            igracBrojac--;
            console.log("Uz osmicu igrača bačena je: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
            postaviGornju(gornjaKarta);
          }, 400);
        }
      }
      if (gornjaKarta.vrijednost == "sedam") {
        brojac7 += 2;
      }
    }

    //ako igrač odabere as
    else if (odabranaKarta.vrijednost == "as") {
  //    console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKarta;
      igracBrojac--;
    }

    //ako igrač odabere dečka
    else if (odabranaKarta.vrijednost == "decko") {

      if (odabranaKarta.boja == gornjaKarta.boja) {
        pauza=true;
        document.getElementById('izbor').style.display='block';
      }
      if (odabranaKarta.boja == "srce") {
        brojac7=0;
      }
////      console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKarta;
      igracBrojac--;
    }

    //ako igrač odabere bilo što drugo iste boje ili vrijednosti
    else {
////      console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKarta;
      igracBrojac--;
////      console.log(gornjaKarta.boja+" "+gornjaKarta.vrijednost+" je nova gornja karta.");

    }
  }

  //ako igrač odabere dečka
  else if (odabranaKarta.vrijednost == "decko") {
    if (odabranaKarta.boja == "srce") {
      brojac7=0;
    }
//    console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
    odbacene.push(gornjaKarta);
    gornjaKarta=odabranaKarta;
    igracBrojac--;
  }

  //prikaz gornje karte
  postaviGornju(gornjaKarta);

  if (novaFlag==true) {
    setTimeout(function(){
      $(".boja").empty();
      //novaFlag=false;
    }, 1000);
  }

  //provjera je li igrač ostao bez karata
//  console.log("igrac brojac "+igracBrojac);
  if(igracBrojac <= 0) {
//    console.log("prazan igrac");
    document.getElementById('comp').style.display='none';
    document.getElementById('play').style.display='none';
    document.getElementById('hand').style.display='none';
    document.getElementById('cestitke').style.display='block';
    document.getElementById('btnVuci').style.display='none';
  }

}

function odaberiKartuComp() {
  if (compFlag == true && pauza == false) {
    let odabranaKartaComp;

    //ako je bačena sedmica, AI želi baciti sedmicu ako je moguće ili dečka srce
    if (gornjaKarta.vrijednost == "sedam" && brojac7 != 0) {
  //    console.log("trazi sedmicu na sedmicu");
      for (let i=0; i<rukaComp.length; i++) {
        if (rukaComp[i].bacena == false && (rukaComp[i].vrijednost == "sedam" || (rukaComp[i].vrijednost == "decko" && rukaComp[i].boja == "srce"))) {
          odabranaKartaComp=rukaComp[i];
          rukaComp[i].bacena=true;
          $(".comp-area :last-child").remove();
    //      console.log("nasao sedmicu na sedmicu");
          return odabranaKartaComp;
        }
      }
    }

    //izvlacenje karata zbog sedmice
    if (brojac7 != 0) {
      for (let i=0; i<brojac7; i++) {
        izvuciKartuComp();
      }
      brojac7 = 0;
    }


    //ako je bačen as, AI želi baciti još jedan as
    if (gornjaKarta.vrijednost == "as") {
  ////    console.log("trazi as na as");
      for (let i=0; i<rukaComp.length; i++) {
        if (rukaComp[i].bacena == false && rukaComp[i].vrijednost == "as") {
          odabranaKartaComp=rukaComp[i];
          rukaComp[i].bacena=true;
          $(".comp-area :last-child").remove();
  //        console.log("nasao as na as");
          return odabranaKartaComp;
        }
      }
    }

    //ako je bačeno bilo što drugo, vrijede sljedeći prioriteti:

    //želi baciti sedmicu ako ima bar još jednu sedmicu
    let temp=0;
    let help;
//   console.log("trazi sedmicu");
    for (let i=0; i<rukaComp.length; i++) {
      if (rukaComp[i].bacena == false && rukaComp[i].vrijednost=="sedam") {
        temp++;
        if (rukaComp[i].boja == gornjaKarta.boja) {
          help=i;
        }
      }
    }
    if (temp>=2 && help) {
      odabranaKartaComp = rukaComp[help];
      rukaComp[help].bacena=true;
      $(".comp-area :last-child").remove();
  //    console.log("nasao sedmicu");
      return odabranaKartaComp;
    }

    //želi baciti as ako ima nešto iste boje ili još jedan as i nešto te druge boje
    let asevi = [];
//    console.log("trazi as");
    for (let i=0; i<rukaComp.length; i++) {
      if (rukaComp[i].bacena == false && rukaComp[i].vrijednost == "as") {
        asevi.push(rukaComp[i].boja);
      }
    }
    if (asevi.length >= 2) { //ako ima dva asa
      temp=0;
      let flag=false;
      for (let i = 0; i<asevi.length; i++) {
        if (asevi[i] != gornjaKarta.boja) {
          for (let j = 0; j<rukaComp.length; j++) {
            if (asevi[i] == rukaComp[j].boja && rukaComp[j].vrijednost != "as") {
              temp++;
            }
          }
        }
        else {
          flag=true;
        }
      }
      if (temp>0 && flag == true) {
        for (let i=0; i<rukaComp.length; i++) {
          if (rukaComp[i].bacena == false && rukaComp[i].boja == gornjaKarta.boja && rukaComp[i].vrijednost == "as") {
            odabranaKartaComp=rukaComp[i];
            rukaComp[i].bacena=true;
            $(".comp-area :last-child").remove();
  //          console.log("nasao as");
            return odabranaKartaComp;
          }
        }
      }
    }
    else if (asevi.length == 1 && asevi[0] == gornjaKarta.boja) {
      temp=0;
      for (let i=0; i<rukaComp.length; i++) {
        if (rukaComp[i].bacena == false && rukaComp[i].boja == gornjaKarta.boja && rukaComp[i].vrijednost != "as") {
          temp++;
        }
      }
      if (temp>0) {
        for (let i=0; i<rukaComp.length; i++) {
          if (rukaComp[i].bacena == false && rukaComp[i].vrijednost == "as") {
            odabranaKartaComp=rukaComp[i];
            rukaComp[i].bacena=true;
            $(".comp-area :last-child").remove();
  //          console.log("nasao as");
            return odabranaKartaComp;
          }
        }
      }
    }

    //želi baciti osmicu
//    console.log("trazi osmicu");
    for (let i=0; i<rukaComp.length; i++) {
      if (rukaComp[i].bacena == false && rukaComp[i].boja == gornjaKarta.boja && rukaComp[i].vrijednost == "osam") {
        odabranaKartaComp=rukaComp[i];
        rukaComp[i].bacena=true;
        $(".comp-area :last-child").remove();
    //    console.log("nasao osmicu");
        return odabranaKartaComp;
      }
    }

    //želi baciti nešto iste boje ili vrijednosti
//    console.log("trazi boju ili vrijednost");
    for (let i=0; i<rukaComp.length; i++) {
      if (rukaComp[i].vrijednost != "decko" && rukaComp[i].bacena == false && ((novaFlag == false && rukaComp[i].boja == gornjaKarta.boja) || (novaFlag == true && rukaComp[i].boja ==novaBoja) || rukaComp[i].vrijednost == gornjaKarta.vrijednost)) {
        odabranaKartaComp=rukaComp[i];
  //      console.log(odabranaKartaComp.boja+" "+odabranaKartaComp.vrijednost+" je odabrana");
        rukaComp[i].bacena=true;
        $(".comp-area :last-child").remove();
  //      console.log("nasao boju vrijednost");
        return odabranaKartaComp;
      }
    }

    //želi baciti dečka ako nema što drugo
//    console.log("trazi decka");
    for (let i=0; i<rukaComp.length; i++) {
      if (rukaComp[i].bacena == false && rukaComp[i].vrijednost == "decko") {
        odabranaKartaComp=rukaComp[i];
        rukaComp[i].bacena=true;
        $(".comp-area :last-child").remove();
  //      console.log("nasao decka");
        return odabranaKartaComp;
      }
    }

    //ako dosad funkcija nije ništa vratila, znači da comp nema karata za baciti - izvlači kartu
//    console.log("treba vuci kartu");
      izvuciKartuComp();
      $(".broj-spil").empty().append(karte.length);
      odabranaKartaComp=odaberiKartuComp();
      potezComp(odabranaKartaComp);
  }
}

function izvuciKartuComp() {
  if (karte.length == 0) {
    promijesajSpil();
  }
  rukaComp.push(karte[karte.length-1]);
  compBrojac++;

  console.log("komp je izvukao "+karte[karte.length-1].boja+" "+karte[karte.length-1].vrijednost);
  let izvKartaComp = $("<input/>", {type: "image", id: "comp"+(rukaComp.length-1), src: "images\\back.png", width:"98", height:"150"});
  $(".comp-area").append(izvKartaComp);
  karte.pop();
}

//potez racunala
function potezComp(odabranaKartaComp) {

  if (odabranaKartaComp.vrijednost==gornjaKarta.vrijednost || (novaFlag == false && odabranaKartaComp.boja==gornjaKarta.boja) || (novaFlag == true && odabranaKartaComp.boja == novaBoja)) {
    novaFlag=false;
    //ako comp odabere sedmicu
    if (odabranaKartaComp.vrijednost == "sedam") {
      brojac7 += 2;
//      console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKartaComp;
      compBrojac--;
    }

  //ako comp odabere osmicu
    else if (odabranaKartaComp.vrijednost == "osam") {
//      console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKartaComp;
      compBrojac--;
      let priv=rukaComp.length;
      for (let i=0; i<priv; i++) {
        if (rukaComp[i].boja == odabranaKartaComp.boja && rukaComp[i].bacena == false) {

//            console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
            odbacene.push(gornjaKarta);
            gornjaKarta=rukaComp[i];
            compBrojac--;
            console.log("Uz osmicu računala bačena je: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
            postaviGornju(gornjaKarta);
            rukaComp[i].bacena=true;
            $(".comp-area :last-child").remove();

        }
      }
      if (gornjaKarta.vrijednost == "sedam") {
        brojac7 += 2;
      }
      else if (gornjaKarta.vrijednost == "as") {
        odabranaKartaComp=odaberiKartuComp();
        potezComp(odabranaKartaComp);
      }
    }

  //ako comp odabere as
    else if (odabranaKartaComp.vrijednost == "as") {
//      console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKartaComp;
      compBrojac--;
    }

    //ako comp odabere dečka
    else if (odabranaKartaComp.vrijednost == "decko") {
      if (odabranaKartaComp.boja == gornjaKarta.boja) {
        let temp;
        switch(odabranaKartaComp.boja) { //ovo je potrebno kako random ne bi mogao odabrati boju koja je već odabrana
          case "zir":
            temp=1;
            break;
          case "list":
            temp=2;
            break;
          case "bundeva":
            temp=3;
            break;
          case "srce":
            temp=4;
            break;
        }
        let rand;
        do {
          rand = Math.floor((Math.random()*4)+1);
        } while (rand==temp);

        switch(rand) { //odabir nove boje
          case 1:
            izborZir();
            break;
          case 2:
            izborList();
            break;
          case 3:
            izborBundeva();
            break;
          case 4:
            izborSrce();
            break;
        }
      }
      if (odabranaKartaComp.boja == "srce") {
        brojac7=0;
      }
  //    console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKartaComp;
      compBrojac--;
    }

    else {
  //    console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
      odbacene.push(gornjaKarta);
      gornjaKarta=odabranaKartaComp;
      compBrojac--;
    }
  }

  //ako comp odabere dečka
  else if (odabranaKartaComp.vrijednost == "decko") {
    if (odabranaKartaComp.boja == "srce") {
      brojac7=0;
    }
//    console.log("odbacena: "+gornjaKarta.boja+" "+gornjaKarta.vrijednost);
    odbacene.push(gornjaKarta);
    gornjaKarta=odabranaKartaComp;
    compBrojac--;
  }

//  console.log("comp je odabrao: "+odabranaKartaComp.boja+" "+odabranaKartaComp.vrijednost);

  postaviGornju(gornjaKarta);

  if (novaFlag==true) {
    setTimeout(function(){
      $(".boja").empty();
    //  novaFlag=false;
    }, 1000);
  }

  if (gornjaKarta.vrijednost != "as") {
    compFlag=false;
    igracFlag=true;
    console.log(igracFlag);
  }
  else {
    odabranaKartaComp=odaberiKartuComp();
    potezComp(odabranaKartaComp);
  }
//  console.log("comp brojac "+compBrojac);
  if(compBrojac <= 0) {
//    console.log("prazan comp");
    document.getElementById('comp').style.display='none';
    document.getElementById('play').style.display='none';
    document.getElementById('hand').style.display='none';
    document.getElementById('game-over').style.display='block';
    document.getElementById('btnVuci').style.display='none';
  }
}

//funkcija se poziva kad u spilu vise nema karata
function promijesajSpil() {
  for (let i=0; i<odbacene.length; i++) {
    karte.push(odbacene[i]);
    karte[i].bacena=false;
  }
  odbacene.length=0;
  mijesanje();
  $(".broj-spil").empty().append(karte.length);
}

function izvuciKartu(id) {
  if (igracFlag == true || rukaIgrac.length < 7) {
    rukaIgrac.push(karte[karte.length-1]);
    igracBrojac++;
    let izvKarta = $("<input/>", {type: "image", id: (rukaIgrac.length-1), src: rukaIgrac[rukaIgrac.length-1].slika, width:"98", height:"150"});
    $(".hand-area").append(izvKarta);
   document.getElementById(id).onclick=function() {
     if (igracFlag == true) {
        let odabranaKarta = rukaIgrac[id];

        //izvlacenje karata zbog sedmice
        if (brojac7 != 0 && (odabranaKarta.vrijednost != "sedam" && (odabranaKarta.boja != "srce" || odabranaKarta.vrijednost != "decko"))) {
          for (let i=0; i<brojac7; i++) {
            izvuciKartu(rukaIgrac.length);
          }
          brojac7 = 0;
        }

        rukaIgrac[id].bacena=true;
        if (odabranaKarta.vrijednost == gornjaKarta.vrijednost || odabranaKarta.vrijednost == "decko" || (novaFlag == false && odabranaKarta.boja == gornjaKarta.boja) || (novaFlag == true && odabranaKarta.boja == novaBoja)) {
  //        console.log("igrac je odabrao: "+odabranaKarta.boja+" "+odabranaKarta.vrijednost);
          document.getElementById(id).remove();
          potezIgrac(odabranaKarta, gornjaKarta);
          if (gornjaKarta.vrijednost != "as") {
            igracFlag=false;
            compFlag=true;
            console.log(igracFlag);
          }
          setTimeout( function() {
            let odabranaKartaComp=odaberiKartuComp();
            potezComp(odabranaKartaComp);
          }, 1000);
        };
      }
    };
    karte.pop();
    $(".broj-spil").empty().append(karte.length);
    if (karte.length == 0) {
      promijesajSpil();
      izvuciKartu(id);
    }
  }
}

function izborZir() {
  if (gornjaKarta.boja != "zir") {
    novaBoja="zir";
    novaFlag=true;
    $(".boja").empty().append('<img src="images\\zir.png" />');
    document.getElementById('izbor').style.display='none';
    if (pauza == true) {
      pauza = false;
      let odabranaKartaComp = odaberiKartuComp();
      potezComp(odabranaKartaComp);
    }
  }
}

function izborBundeva() {
  if (gornjaKarta.boja != "bundeva") {
    novaBoja="bundeva";
    novaFlag=true;
    $(".boja").empty().append('<img src="images\\bundeva.gif" />');
    document.getElementById('izbor').style.display='none';
    if (pauza == true) {
      pauza = false;
      let odabranaKartaComp = odaberiKartuComp();
      potezComp(odabranaKartaComp);
    }
  }
}

function izborSrce() {
  if (gornjaKarta.boja != "srce") {
    novaBoja="srce";
    novaFlag=true;
    $(".boja").empty().append('<img src="images\\srce.gif" />');
    document.getElementById('izbor').style.display='none';
    if (pauza == true) {
      pauza = false;
      let odabranaKartaComp = odaberiKartuComp();
      potezComp(odabranaKartaComp);
    }
  }
}

function izborList() {
  if (gornjaKarta.boja != "list") {
    novaBoja="list";
    novaFlag=true;
    $(".boja").empty().append('<img src="images\\list.gif" />');
    document.getElementById('izbor').style.display='none';
    if (pauza == true) {
      pauza = false;
      let odabranaKartaComp = odaberiKartuComp();
      potezComp(odabranaKartaComp);
    }
  }
}

function main() {
  document.getElementById('comp').style.display='block';
  document.getElementById('play').style.display='block';
  document.getElementById('hand').style.display='block';
  document.getElementById('pokreni').style.display='none';
  document.getElementById('btnPokreni').style.display='none';
  document.getElementById('btnVuci').style.display='block';

  igracFlag = Math.random() >= 0.5;
  compFlag = !(igracFlag);
  console.log(igracFlag);

  mijesanje();

  //postavljanje prve karte na igraće polje
  gornjaKarta=karte[karte.length-1];
  postaviGornju(gornjaKarta);
  karte.pop(); //izbacivanje karte iz špila

  //podjela karata: 7 igracu
  for(let i=0; i<7; i++) {
    izvuciKartu(rukaIgrac.length);
  }
  //i 7 racunalu (drugom igracu)
  for (let i=0; i<7; i++) {
    rukaComp.push(karte[karte.length-1]);
    compBrojac++;
//    console.log("komp je izvukao "+karte[karte.length-1].boja+" "+karte[karte.length-1].vrijednost);
    let izvKartaComp = $("<input/>", {type: "image", id: "comp"+(rukaComp.length-1), src: "images\\back.png", width:"98", height:"150"});
    $(".comp-area").append(izvKartaComp);
    karte.pop();
  }
  $(".broj-spil").empty().append(karte.length);

  if (compFlag == true) {
    setTimeout( function() {
      let odabranaKartaComp=odaberiKartuComp();
      potezComp(odabranaKartaComp);
    }, 1000);
  }

}
